
/*
{
  "key" : "key"
  "value" : "english word",
  "translate" : [
    "phrase_sinonym_key1",
    "phrase_synonim_key2"
  ],
  "removed" : "false"
}
*/

function addPhrase(newPhrase)
{
  phrases.push(newPhrase);
}

function editPhrase(renewed)
{
  for(var p in phrases)
  {
    if(p.key === renewed.key)
    {
      p.value = renewed.value;
      p.translate = renewed.translate;
      return true;
    }
  }
  
  return false;
}

function removePhrase(forDelete)
{
  for(var p in phrases)
  {
    if(p.key === renewed.key)
    {
      p.removed = true;
      return true;
    }
  }
  
  return false;
}